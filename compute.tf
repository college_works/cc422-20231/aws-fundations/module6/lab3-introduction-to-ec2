resource "aws_instance" "webserver" {
  ami           = "ami-06e46074ae430fba6" # us-west-1 - Amazon Linux 2023 AMI
  instance_type = "t2.small"
  key_name = "vockey"
  disable_api_termination = true
  associate_public_ip_address = true
  vpc_security_group_ids = [aws_security_group.allow_http.id]
  user_data = <<-EOF
    #!/bin/bash

    yum -y install httpd
    systemctl enable httpd
    systemctl start httpd
    echo '<html><h1>Hello From Your Web Server!</h1></html>' > /var/www/html/index.html
  EOF

  root_block_device {
    volume_size = 10
    volume_type = "gp3"
    encrypted = false
  }

  tags = {
    Name = "Web Server"
  }
}
